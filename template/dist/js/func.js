    function dateShow(objReference) {
        $(objReference).datepicker('show');
    }

    function formatCurrency(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
        var num = parseFloat(amount); //convert to float  
        //default values  
        decimalSeparator = decimalSeparator || '.';
        thousandsSeparator = thousandsSeparator || ',';
        nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

        var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits  
        //separate begin [$1], middle [$2] and decimal digits [$4]  
        var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);
        
        var returnValue = '0';

        if (parts) { //num >= 1000 || num < = -1000  
            returnValue = parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
        } else {
            returnValue =  fixed.replace('.', decimalSeparator);
        }

        if (num<0)
        {
            returnValue = "("+returnValue+")";
            returnValue = returnValue.replace(/-/g, '');
        }

        return returnValue;
    }

    function strToCurr(strCurrency) {
        var strNewValue = '0';

        if (strCurrency.trim().substring(0,1) == '(')
        {
            strNewValue = strCurrency.replace(/\(/g, '');
            strNewValue = strNewValue.replace(/\)/g, '');
            strNewValue = strNewValue.replace(/,/g, '');
            strNewValue = parseFloat(strNewValue) * -1;
        }
        else
        {
            strNewValue = strCurrency.replace(/,/g, '');
            strNewValue = parseFloat(strNewValue);
        }

        return strNewValue;
    }

    $(document).ready(function() {
      
        initDatePicker();
        initAutoNumeric();
        
       // $('label[role="notEmpty"]').attr("style","text-decoration:underline;");
       $('label[role="notEmpty"]').prepend('<b>* </b>');
    });

    function initDatePicker()
    {
        $('[role="date"]').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true
        });

    }

    function initAutoNumeric()
    {

        $('[role="numeric"]').blur( function(e)
        { 
            checkEmpty($(this));
        });

        $('[role="numeric"]').autoNumeric("init",{
            aSep: ',',
            aDec: '.', 
            nBracket: '(,)',
            vMax : 99999999999999999999,
            mDec: 2
        });
    }


    function setCookie(cname, cvalue) {
        var d = new Date();
        var exdays = 30;
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        //document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
        document.cookie = cname + "=" + cvalue + "; path=/";
    } 

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }

        return "";
    } 

    function getSearchDate(cName, cDefaultValue)
    {
        var strName = getCookie(cName);

        (strName == '') ? setCookie(cName, cDefaultValue) : '';

        return getCookie(cName);
    }

    function numRows(objSource, colNo) {
        var table = objSource;
        for (i = 0; i < table.rows.length; i++) {
            table.rows[i].cells[colNo].innerHTML = i + 1;
        }
    }

    function clearGrid(objSource) {
        var table = objSource;
        for (i = 0; i < table.rows.length; i++) {
            table.deleteRow(i);
        }
    }

    function setDatePicker(objReference, tanggal)
    {
        var arrTanggal = tanggal.split('-');
        var day     = arrTanggal[0];
        var month   = (parseInt(arrTanggal[1]) - 1).toString();
        var year    = arrTanggal[2];

        $(objReference).datepicker('update', new Date(year, month, day));
    
    }

    function checkEmpty(objSource)
    {
        var strValue   = $(objSource).val().trim();

        strValue = (strValue == '') ? formatCurrency(0, '.', ',', decimalDigit) : strValue;

        $(objSource).val(strValue);
   
    }