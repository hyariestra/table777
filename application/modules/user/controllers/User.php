<?php 

/**
* 
*/
class user extends CI_controller
{

	function __construct()

	{

		parent::__construct();

		if (!$this->session->userdata("pengguna"))

		{

			redirect("pengguna/login");

		}

		$this->load->model('M_user');
		$this->load->library('form_validation');


	}
	
	function index()

	{
		authorize('pengguna');
		$data['judul']="Daftar Pengguna";

		$data['act'] = 'user';

		$data['admin']=$this->M_user->tampil_admin();

		$this->themeadmin->tampilkan('tampilpengguna',$data);

	}

	function tambah()

	{
		authorize('pengguna');
		$data['judul']="Tambah Pengguna";

		$data['act'] = 'userTambah';

		$data['roles'] =$this->M_user->getRoles();

		$this->themeadmin->tampilkan('tambahpengguna',$data);

	}

	function simpan()
	{
		$this->load->model("M_user");

		$this->form_validation->set_rules('email','Username','required');

		if ($this->form_validation->run() != false) {
			$datainputan=$this->input->post();

			$hasil  = $this->M_user->simpanPengguna($datainputan);

			if ($hasil=="gagal") 
			{
				$isipesan="<br><div class='alert alert-danger'>Username Sudah Terdaftar, Gunakan Username Lain!</div>";
				$this->session->set_flashdata("pesan",$isipesan);
				redirect("user/tambah");
			}
			else
			{
				$isipesan="<br><div class='alert alert-success'>Data Pengguna Berhasil ditambah</div>";
				$this->session->set_flashdata("pesan",$isipesan);
				redirect("user");
			}
		} else {
			$isipesan="<br><div class='alert alert-danger'>Username Tidak Boleh Kosong, Harap Isi Username!</div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect("user/tambah");
		}
		




	}

	function hapus($id)

	{

		$this->M_user->hapus_pengguna($id);

		redirect('user');

	}

	function ubah($id)

	{
		authorize('pengguna');
		$this->load->model("M_user");

		$inputan=$this->input->post();

		if ($inputan) 

		{

			$this->M_user->ubah_admin($inputan,$id);
			$isipesan="<br><div class='alert alert-success'>Data Berhasil Diubah </div>";
			$this->session->set_flashdata("pesan",$isipesan);
			redirect('user');

		}

		$data['judul']="Ubah Pengguna";

		$data['admin']=$this->M_user->ambil_admin($id);

		$data['roles'] =$this->M_user->getRoles();

		$this->themeadmin->tampilkan("ubahpengguna",$data);

	}

	function profile($id)
	{
		$idAdmin = $this->session->userdata['pengguna']['id_admin'];

		if ($idAdmin==$id) {

			$inputan=$this->input->post();

			if ($inputan) 

			{

				$this->M_user->ubah_admin($inputan,$id);
				$isipesan="<br><div class='alert alert-success'>Data Berhasil Diubah </div>";
				$this->session->set_flashdata("pesan",$isipesan);
				redirect('user/profile/'.$id);

			}
			$data['judul']="Ubah Pengguna";

			$data['admin']=$this->M_user->ambil_admin($id);

			$data['roles'] =$this->M_user->getRoles();

			$this->themeadmin->tampilkan("ubahpengguna",$data);
		}else{
			redirect("dashboard/notFound");
		}
	}
}

?>