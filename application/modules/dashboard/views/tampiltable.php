<style>
	#MyTable td:hover { 
		background-color: #ccc;
	}

	table, th, td {
		border: 1px solid black;
	}
	table{
		
		text-align: center;
	}

	tr.even{
		background-color: #E8E4E7;
	}
</style>

<div class="row">

	<div class="col-md-12">

		<div class="box">

			
			<div class="box-body">


				<table width="100%"  id="MyTable" >
					<tr>
						<?php
						$counter=1;
						foreach ($query as $key => $value){

							if ($counter % 5 ==0) {
								$color = '#E8E4E7';
								$cok = 'background-color:white';
							}else{
								$color = '#black';
								$cok = 'background-color: ';
							}


							echo '<td  style='.$cok.';color:'.$color.'>'.$value['number'].'</td>';
							echo "\n";

							if ($counter % 35 == 0 && $counter !== 0) {
								echo '</tr><tr>';
							}      

							$counter++;

						}
						?>
					</tr>
				</table>

				<br>

				<form action="<?php echo base_url("dashboard/simpan") ?>" method="POST" >

					<input  required="" placeholder="angka 1" name="angka[]" type="number">					
					<input  required="" placeholder="angka 2" name="angka[]" type="number">					
					<input  required="" placeholder="angka 3" name="angka[]" type="number">					
					<input  required="" placeholder="angka 4" name="angka[]" type="number">	

					<br>
					<br>
					<button type="submit" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah</button>
				</form>
				<br>
				<form action="<?php echo base_url('dashboard/HapusData'); ?>" method="post" accept-charset="utf-8">
					<button type="submit" class="button_delete btn btn-danger"><i class="fa fa-undo" aria-hidden="true"></i> Hapus
					</button>
				</form>

			</div>


			

		</div>

	</div>

</div>




<script>
	
	$( document ).ready(function() {

		var table = document.getElementById("MyTable");   
		var rows = table.getElementsByTagName("tr");   


		console.log(rows.length);
		for(i = 0; i < rows.length; i++){    

			

			if((i+1) % 6 == 0){ 
				rows[i].className = "even"; 
			}else{ 
				rows[i].className = "odd"; 
			}   


			if(i == 0){ 
				rows[i].className = "even"; 
			}   

			
		} 
	});




	$('.button_delete').on('click',function(e){
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
			title: "Apa Anda Yakin Menghapus 5 Data Terakhir? ",
			text: "Data yang telah dihapus tidak dapat dikembalikan",
			type: "error",
			showCancelButton: true,
			cancelButtonClass: 'btn-default btn-md waves-effect',
			confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
			confirmButtonText: 'Delete!'
		}, 
		function(willDelete) {
			if (willDelete) {
				form.submit();

			}     
		});
	});


</script>
