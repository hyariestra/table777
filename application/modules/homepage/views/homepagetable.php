<!DOCTYPE html>
<head>
	<meta charset='UTF-8' />
	
	<title>Drawing Table</title>

	<link rel="stylesheet" href="<?php echo base_url("template/bootstrap/css/bootstrap.min.css") ?>">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url("template/plugins/drawTable/css/colorpicker.css") ?>">
	<link rel="stylesheet" href="<?php echo base_url("template/plugins/drawTable/css/style.css") ?>">
	
	<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js'></script>
<!-- 	<script src='js/colorpicker.js'></script>
	<script src='js/drawingtable.js'></script> -->

	<script src="<?php echo base_url("template/plugins/drawTable/js/drawingtable.js") ?>"></script>
	<script src="<?php echo base_url("template/plugins/drawTable/js/colorpicker.js") ?>"></script>

</head>


<div class="container">
	
	<div style="margin-top: 40px;" class="col-md-12">

		<div class="col-md-3">

		</div>

		<div class="col-md-6">
			
			

			<fieldset style="display: none;" id="grid-size-wrap">
				<legend>Grid Size</legend>
				
				
				<select id="gridSize">
					<option value="10,10">10 x 10</option>
					<option value="20,20" selected>20 x 20</option>
					<option value="30,30">30 x 30</option>
				</select>
				<br />
				Changing size clears the design
			</fieldset>

			<fieldset id="color-selector">
				
				<div class="color red selected" data-color="red"><input type="text"></div>
				<div class="color green" data-color="green"><input type="text"></div>
				<div class="color blue" data-color="blue"><input type="text"></div>
				<div class="color pink" data-color="pink"><input type="text"></div>
				<div class="color yellow" data-color="yellow"><input type="text"></div>
				<div class="color eraser" data-color="eraser"></div>

				<button id="clear">Clear the Color</button>
			</fieldset>
			

			
			

			<div class="ml-anno">GUBUK 303</div>
			
			<div id="table-wrap">
				<table cellpadding="10" id="drawing-table">

					
				</table>

			</div>
			
		</div>

		<div class="col-md-3">
			


		</div>

	</div>


</div>

<script>

	$( document ).ready(function() {

		var table = document.getElementById("drawing-table");   
		var rows = table.getElementsByTagName("tr");   


		console.log(rows.length);
		for(i = 0; i < rows.length; i++){    

			

			if((i+1) % 6 == 0){ 
				rows[i].className = "even"; 
			}else{ 
				rows[i].className = "odd"; 
			}   


			if(i == 0){ 
				rows[i].className = "even"; 
			}   

			
		} 
	});



	var cols = 20,
	rows = 20,
	curColor = "red",
	mouseDownState = false,
	eraseState = false,
	tracingMode = false,
	prevColor = "",
	$el;



	function buildGrid(cols, rows) {

		var satuan =  <?= json_encode($table); ?>;

		console.log(satuan);

		var tableMarkup = "";

		tableMarkup = satuan;

		$("#drawing-table").html(tableMarkup)

	};


	
</script>


</html>